# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/dotNET-Core.gitlab-ci.yml

# This is a simple example illustrating how to build and test .NET Core project
# with GitLab Continuous Integration / Continuous Delivery.
#
# ### Specify the Docker image
#
# Instead of installing .NET Core SDK manually, a docker image is used
# with already pre-installed .NET Core SDK.
#
# The 'latest' tag targets the latest available version of .NET Core SDK image.
# If preferred, you can explicitly specify version of .NET Core (e.g. using '2.2-sdk' tag).
#
# See other available tags for .NET Core: https://hub.docker.com/_/microsoft-dotnet
# Learn more about Docker tags: https://docs.docker.com/glossary/?term=tag
# and the Docker itself: https://opensource.com/resources/what-docker
image: mcr.microsoft.com/dotnet/sdk:latest

# ### Define variables
#
variables:
  # 1) Name of directory where restore and build objects are stored.
  OBJECTS_DIRECTORY: 'obj'
  # 2) Name of directory used for keeping restored dependencies.
  NUGET_PACKAGES_DIRECTORY: '.nuget'
  # 3) A relative path to the source code from project repository root.
  # NOTE: Please edit this path so it matches the structure of your project!
  SOURCE_CODE_PATH: '*/*/Tailspin.SpaceGame.Web.csproj'

# ### Define global cache rule
#
# Before building the project, all dependencies (e.g. third-party NuGet packages)
# must be restored. Jobs on GitLab.com's Shared Runners are executed on autoscaled machines.
#
# Each machine is used only once (for security reasons) and after that is removed.
# This means that, before every job, a dependency restore must be performed
# because restored dependencies are removed along with machines. Fortunately,
# GitLab provides cache mechanism with the aim of keeping restored dependencies
# for other jobs.
#
# This example shows how to configure cache to pass over restored
# dependencies for re-use.
#
# With global cache rule, cached dependencies will be downloaded before every job
# and then unpacked to the paths as specified below.
cache:
  # Per-stage and per-branch caching.
  key: "$CI_JOB_STAGE-$CI_COMMIT_REF_SLUG"
  paths:
    # Specify three paths that should be cached:
    #
    # 1) Main JSON file holding information about package dependency tree, packages versions,
    # frameworks etc. It also holds information where to the dependencies were restored.
    - '$SOURCE_CODE_PATH$OBJECTS_DIRECTORY/project.assets.json'
    # 2) Other NuGet and MSBuild related files. Also needed.
    - '$SOURCE_CODE_PATH$OBJECTS_DIRECTORY/*.csproj.nuget.*'
    # 3) Path to the directory where restored dependencies are kept.
    - '$NUGET_PACKAGES_DIRECTORY'
  #
  # 'pull-push' policy means that latest cache will be downloaded (if it exists)
  # before executing the job, and a newer version will be uploaded afterwards.
  # Such a setting saves time when there are no changes in referenced third-party
  # packages.
  #
  # For example, if you run a pipeline with changes in your code,
  # but with no changes within third-party packages which your project is using,
  # then project restore will happen quickly as all required dependencies
  # will already be there — unzipped from cache.

  # 'pull-push' policy is the default cache policy, you do not have to specify it explicitly.
  policy: pull-push

# ### Restore project dependencies
#
# NuGet packages by default are restored to '.nuget/packages' directory
# in the user's home directory. That directory is out of scope of GitLab caching.
#
# To get around this, a custom path can be specified using the '--packages <PATH>' option
# for 'dotnet restore' command. In this example, a temporary directory is created
# in the root of project repository, so its content can be cached.
#
# Learn more about GitLab cache: https://docs.gitlab.com/ee/ci/caching/index.html
before_script:
  - 'dotnet restore --packages $NUGET_PACKAGES_DIRECTORY'

stages:          # List of stages for jobs, and their order of execution
  - build
  - test
  - push
  - deploy

build-job:       # This job runs in the build stage, which runs first.
  stage: build
  script:
    - 'dotnet build --no-restore'


unit-test-job:   # This job runs in the test stage.
  stage: test    # It only starts when the job in the build stage completes successfully.
  script:
    - echo "Running unit tests... This will take about 60 seconds."
    - sleep 60
    - echo "Code coverage is 90%"
    - 'dotnet test --no-restore'


lint-test-job:   # This job also runs in the test stage.
  stage: test    # It can run at the same time as unit-test-job (in parallel).
  script:
    - echo "Linting code... This will take about 10 seconds."
    - sleep 10
    - echo "No lint issues found."

docker-push:
  # Use the official docker image.
  image: docker:cli
  stage: push
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" 
  script:
    - docker build --pull -t sherifsameh/dotnet-app .
    - docker push sherifsameh/dotnet-app:latest
   

deploy-local:      # This job runs in the deploy stage.
  stage: deploy  # It only runs when *both* jobs in the test stage complete successfully.
  environment: production
  script:
    - echo "Deploying application..."
    - echo "Application successfully deployed."

deploy-k8s:
  stage: deploy
  image: dtzar/helm-kubectl
  script:
    - kubectl config set-cluster k8s --server="${SERVER}"
    - kubectl config set clusters.k8s.certificate-authority-data ${CERTIFICATE_AUTHORITY_DATA}
    - kubectl config set-credentials gitlab --token="${USER_TOKEN}"
    - kubectl config set-context default --cluster=k8s --user=gitlab
    - kubectl config use-context default
    - sed -i "s/<VERSION>/${CI_COMMIT_SHORT_SHA}/g" deployment.yaml
    - kubectl apply -f deployment.yaml